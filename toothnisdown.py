#!/usr/bin/env python3
import os
import sys
import time
import math
import re
import logging
import requests
from pathlib import Path
from dotenv import load_dotenv,find_dotenv
from mastodon import Mastodon



# load settings from dot-env
if find_dotenv() == "":
    print("You need to create a .env file with some settings. See .env.dist file.")
    sys.exit(-1)
load_dotenv(find_dotenv(), verbose=True)

APPNAME="isdownhn"
LASTFILE=os.getenv("LASTFILE", "lastupdate.time")
SECRETFILE=os.getenv("SECRETFILE", "clientcred.secret")
BASEURL=os.getenv("BASEURL")
USERNAME=os.getenv("USERNAME")
PASSWORD=os.getenv("PASSWORD")

# logging
LOGLEVEL=getattr(logging, os.getenv('LOGLEVEL', 'INFO').upper())
LOGFILE=os.getenv('LOGFILE')
logging.basicConfig(level=LOGLEVEL, filename=LOGFILE, format="%(asctime)s [%(levelname)s] %(name)s : %(message)s")
logger = logging.getLogger(APPNAME)


# last update
lastupdatefile = Path(LASTFILE)
lastupdate = math.floor(time.time())
if lastupdatefile.exists():
    lastupdate = math.floor(lastupdatefile.stat().st_mtime)
logger.debug("last update timestamp: %s", lastupdate)

# search and filter
searchuri = "http://hn.algolia.com/api/v1/search_by_date?query=%22is%20down%22&restrictSearchableAttributes=title&numericFilters=created_at_i>{}".format(lastupdate)
r = requests.get(searchuri)
data = r.json()

hits = data['hits']
hits = [ hit for hit in hits if re.search(r"is down(\.|,| \[| \(|$)", hit['title']) ]
logger.debug("%s hits match our needs", len(hits))
if (len(hits)==0):
    logger.info("No hits, quit.")
    # update last update datetime
    lastupdatefile.touch()
    sys.exit();

# Mastodon:
# Register app and login
if not os.path.exists(SECRETFILE):
    logger.debug("Register app '%s' to '%s",  APPNAME, BASEURL)
    Mastodon.create_app(APPNAME, api_base_url = BASEURL, to_file = SECRETFILE)

    mastodon = Mastodon(client_id = SECRETFILE, api_base_url = BASEURL)
    mastodon.log_in(USERNAME, PASSWORD, to_file = SECRETFILE)


#toot
mastodon = Mastodon(access_token = SECRETFILE, api_base_url = BASEURL)
for hit in hits:
    hit['url'] = hit['url'] or ("https://news.ycombinator.com/item?id="+hit['objectID'])
    message = "{title}\n{url}\n\n(submited by {author})".format(**hit)
    logger.debug("toot: %r (objectID:%s)", message, hit['objectID'])
    mastodon.toot(message)


# update last update datetime
lastupdatefile.touch()


#https://mastodon.social/@hntooter
#http://mastodonpy.readthedocs.io/en/latest/
